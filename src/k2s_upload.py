import time
import sys
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.support.ui import Select
from setup_browser import setup_chrome, setup_firefox


def waitforelement(driver, id=None, css=None, xpath=None, timeout=30):
    try:
        if css:
            WebDriverWait(driver, timeout).until(lambda driver:driver.find_elements_by_css_selector(css))
        if id:
            WebDriverWait(driver, timeout).until(lambda driver:driver.find_element_by_id(id))
        if xpath:
            WebDriverWait(driver, timeout).until(lambda driver:driver.find_element_by_xpath(xpath))
            return 1
    except TimeoutException as e:
        print('TimeoutException: ' + str(e))
        return


def print_progress(iteration, total, prefix='', suffix='', decimals=1,
                   bar_length=50):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        bar_length  - Optional  : character length of bar (Int)
    """
    str_format = "{0:." + str(decimals) + "f}"
    percents = str_format.format(100 * (iteration / float(total)))
    filled_length = int(round(bar_length * iteration / float(total)))
    bar = '=' * filled_length + '-' * (bar_length - filled_length)

    sys.stdout.write('\r%s |%s| %s%s %s  ' % (prefix, bar, percents, '%',
                     suffix))

    if iteration == total:
        sys.stdout.write('\n')
    sys.stdout.flush()


def convert_to_Mb(string):
    string = string.strip()
    num = float(string[:-2])
    if string[-2] == 'M':
        return num
    elif string[-2] == 'G':
        return num * 1024
    elif string[-2] == 'K':
        return num / 1024


def monitor_upload(browser):
    printed_title = False
    while True:
        try:
            upload_speed = browser.find_element_by_class_name(
                'qq-upload-speed').text.strip()
            custom_size = browser.find_element_by_class_name(
                'qq-upload-custom-size').text.strip()
            title = browser.find_element_by_class_name(
                'qq-upload-file').text
            if custom_size == '':
                continue
        except WebDriverException:
            time.sleep(0.5)
            continue

        if not printed_title:
            print('Uploading: ' + title)
            printed_title = True
        main_dl, main_total = custom_size.split('/')
        dl, total = map(convert_to_Mb, [main_dl, main_total])
        suffix = '(' + main_total + ') ' + upload_speed
        time.sleep(1)
        print_progress(dl, total, suffix=suffix, bar_length=40)
        if dl == total:
            time.sleep(4)
            print('Upload Complete!')
            break


def upload(file_path):
	LOGIN = "https://k2s.cc/auth/login"
	LOGOUT = "https://k2s.cc/auth/logout"
	EMAIL = "bader77uk@gmail.com"
	PASSWORD = "looping13B/"

	filename = file_path.split("/")[-1]

	chrome = setup_chrome()

	# Uploader start
	print("Keep2share Uploader..")
	time.sleep(2)

	# Open Browser
	chrome.get(LOGIN)
	print("Chrome Browser Launched..")

	# Logging In
	waitforelement(chrome, css="div.form-group.email.active", timeout=30)
	waitforelement(chrome, css="div.form-group.password.active", timeout=30)
	username_input = chrome.find_element_by_css_selector('div.form-group.email.active input.form-control')
	password_input = chrome.find_element_by_css_selector('div.form-group.password.active input.form-control')
	username_input.send_keys(EMAIL)
	password_input.send_keys(PASSWORD)
	login_button = chrome.find_element_by_css_selector('.form-holder .btn.btn-default')
	time.sleep(3)
	login_button.click()
	print('Logged In..')

	# Upload File
	waitforelement(chrome, css="li.my-files", timeout=30)
	my_files = chrome.find_element_by_css_selector("li.my-files")
	my_files.click()
	print("My Files Clicked..")
	waitforelement(chrome, css="div.react-fine-uploader-file-input-container.qq-upload-button-selector input")
	file_input = chrome.find_element_by_css_selector(
    	"div.react-fine-uploader-file-input-container.qq-upload-button-selector input")
	file_input.send_keys(file_path)
	waitforelement(chrome, css='div.check-block label')
	term_checkbox = chrome.find_element_by_css_selector('div.check-block label')
	term_checkbox.click()
	start_button_css = 'div.start-box input.js-grid-start-upload'
	start = chrome.find_element_by_css_selector(start_button_css)
	start.click()

	# Monitor Upload
	# Shows Upload status
	#	monitor_upload(chrome)

	# Close Upload Dialog
	waitforelement(chrome, css='div.start-box input.js-grid-close-upload', timeout=60)
	close_btn_css = 'div.start-box input.js-grid-close-upload'
	close_btn = chrome.find_element_by_css_selector(close_btn_css)
	close_btn.click()
	time.sleep(3)

	# Re-logging
	print('Uploaded File in Process ..')
	chrome.get(LOGOUT)
	time.sleep(3)
	chrome.get(LOGIN)
	waitforelement(chrome, css="div.form-group.email.active", timeout=30)
	waitforelement(chrome, css="div.form-group.password.active", timeout=30)
	username_input = chrome.find_element_by_css_selector('div.form-group.email.active input.form-control')
	password_input = chrome.find_element_by_css_selector('div.form-group.password.active input.form-control')
	username_input.send_keys(EMAIL)
	password_input.send_keys(PASSWORD)
	login_button = chrome.find_element_by_css_selector('.form-holder .btn.btn-default')
	login_button.click()
	waitforelement(chrome, css="li.my-files", timeout=30)
	my_files = chrome.find_element_by_css_selector("li.my-files")
	my_files.click()

	waitforelement(chrome, xpath='//*[@id="main"]/div/div/div/div[2]/div[3]/div/table/tbody/tr[1]/td[2]/div/a', timeout=30)
	elem = chrome.find_element_by_xpath('//*[@id="main"]/div/div/div/div[2]/div[3]/div/table/tbody/tr[1]/td[2]/div/a')
	elem.click()


	waitforelement(chrome, xpath='//*[@id="modal"]/div/div/div/fieldset/div/div/div[1]/div/ul[2]/li', timeout=30)
	link = chrome.find_element_by_xpath('//*[@id="modal"]/div/div/div/fieldset/div/div/div[1]/div/ul[2]/li').text


	print(link)
	chrome.close()
	return link


if __name__ == '__main__':
    # upload('/home/shakib/Downloads/Input-Font.zip')
    #upload('/home28/ahmad63635/test/forum/temp/AbbyWinters.17.09.16.Lucie.L.And.Paulina.Lesbian.XXX.1080p.MP4-KTR/lucie.l.and.paulina.lesbian.mp4')
     upload('/home26/bh/testtest.part02.rar')